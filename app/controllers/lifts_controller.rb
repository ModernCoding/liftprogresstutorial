class LiftsController < ApplicationController

	before_action :set_lift, :only => [:update, :destroy]


	def index
		@lifts = Lift.all
	end


	def create

		@lift = Lift.new(lift_params)

		render @lift.save ? {
	
				:json 		=> 	@lift
	
			} : {

				:status 	=> 	:unprocessable_entity,
				:json 		=> 	@lift.errors

			}

	end


	def update

		render @lift.update(lift_params) ? {
	
				:json 		=> 	@lift
	
			} : {

				:status 	=> 	:unprocessable_entity,
				:json 		=> 	@lift.errors

			}

	end


	def destroy
		@lift.destroy
		head :no_content
	end


	private

		def set_lift
			@lift = Lift.find(params[:id])
		end


		def lift_params

			params.require(:lift).permit(
					:date,
					:liftname,
					:ismetric,
					:weightlifted,
					:repsperformed,
					:onerm
				)

		end

end