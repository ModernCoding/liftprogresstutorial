import React from "react"
import PropTypes from "prop-types"
import ReactDOM from "react-dom"


const coefficients = { 1: 1, 2: .943, 3: .906, 4: .881, 5: .851, 6: .831, 7: .807, 8: .786, 9: .765, 10: .744 };


class Lift extends React.Component {

	constructor(props){
		super(props);

		this.state = {
			edit: false,
			onerm: this.props.lift.onerm,
			ismetric: this.props.lift.ismetric
		};

		this.liftRow = this.liftRow.bind(this);
		this.liftForm = this.liftForm.bind(this);
		this.handleEdit = this.handleEdit.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
		this.handleToggle = this.handleToggle.bind(this);
		this.toggleUnit = this.toggleUnit.bind(this);
		this.reCalculateOneRm = this.reCalculateOneRm.bind(this);
		this.getOneRm = this.getOneRm.bind(this);
	}


	handleEdit(e){
		e.preventDefault();

		const data = {
			date: ReactDOM.findDOMNode(this.refs.date).value,
			liftname: ReactDOM.findDOMNode(this.refs.liftname).value,
			weightlifted: ReactDOM.findDOMNode(this.refs.weightlifted).value,
			ismetric: this.state.ismetric,
			repsperformed: ReactDOM.findDOMNode(this.refs.repsperformed).value,
			onerm: this.state.onerm
		};

		var that = this;

		$.ajax("/lifts/" + that.props.lift.id, {
			type: 'PUT',
			dataType: 'JSON',

			data: {
				lift: data
			},

			success: function(data){
				that.setState({ edit: false });
				that.props.handleEditLift(that.props.lift, data);
			}

		});

	}


	handleDelete(e){
		e.preventDefault();

		var props = this.props;

		$.ajax("/lifts/" + props.lift.id, {
			type: 'DELETE',
			dataType: 'JSON',

			success: function(){
				props.handleDeleteLift(props.lift);
			}

		});
	}


	handleToggle(e){
		e.preventDefault();
		this.setState({ edit: !this.state.edit });
	}


	toggleUnit(e){
		e.preventDefault();
		this.setState({ ismetric: !this.state.ismetric });
	}


	reCalculateOneRm(){

		this.setState({ 
			onerm: this.getOneRm(
					ReactDOM.findDOMNode(this.refs.weightlifted).value,
					ReactDOM.findDOMNode(this.refs.repsperformed).value
				) 
		});
	
	}


	getOneRm(weight, reps){

		switch(false){
			case weight:
			case reps > 0:
			case reps < 11:
				return 0;

			default:
				return weight / coefficients[reps];
		}

	}


	liftRow(){
		return (
			<tr>
				<td>
					<button className='btn btn-info' onClick={this.handleToggle}>
						Edit
					</button>
					<button className='btn btn-danger' onClick={this.handleDelete}>
						Delete
					</button>
				</td>
				
				<td>{this.props.lift.date}</td>
				<td>{this.props.lift.liftname}</td>
				<td>{this.props.lift.weightlifted}</td>
				<td>{this.props.lift.repsperformed}</td>
				<td>{this.props.lift.onerm}</td>
				<td>{this.props.lift.ismetric.toString()}</td>
			</tr>
		);
	}


	liftForm(){
		return (
			<tr>
				<td>
					<button className='btn btn-default' onClick={this.handleToggle}>
						Cancel
					</button>
					<button className='btn btn-success' onClick={this.handleEdit}>
						Save
					</button>
				</td>
				
				<td>
					<input 
						className="form-control"
						type="date"
						defaultValue={this.props.lift.date}
						ref="date"
					/>
				</td>
				
				<td>
					<input 
						className="form-control"
						type="text"
						defaultValue={this.props.lift.liftname}
						ref="liftname"
					/>
				</td>
				
				<td>
					<input 
						className="form-control"
						type="number"
						defaultValue={this.props.lift.weightlifted}
						ref="weightlifted"
						onChange={this.reCalculateOneRm}
					/>
				</td>
				
				<td>
					<input 
						className="form-control"
						type="number"
						min="1"
						max="10"
						defaultValue={this.props.lift.repsperformed}
						ref="repsperformed"
						onChange={this.reCalculateOneRm}
					/>
				</td>

				<td>{this.state.onerm}</td>

				<td>
					<button className="btn btn-primary" onClick={this.toggleUnit}>
						Metric = {this.state.ismetric.toString()}
					</button>
				</td>
			</tr>
		);
	}


	render(){
		return this.state.edit ? this.liftForm() : this.liftRow();
	}
}


export default Lift