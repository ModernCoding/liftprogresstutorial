import React from "react"
import PropTypes from "prop-types"
import OneRmBox from "./OneRmBox"


const coefficients = { 1: 1, 2: .943, 3: .906, 4: .881, 5: .851, 6: .831, 7: .807, 8: .786, 9: .765, 10: .744 };


class LiftForm extends React.Component {

	constructor(props){

		super(props);

		this.state = {
			date: '',
			liftname: '',
			ismetric: false,
			weightlifted: '',
			repsperformed: '',
			onerm: 0,
		};

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleValueChange = this.handleValueChange.bind(this);
		this.toggleUnit = this.toggleUnit.bind(this);

	}


	handleSubmit(e){
		e.preventDefault();

		$.post(
			'', 
			{
			  lift: this.state
			}, 

			(function(_this) {
			  return function(data) {
			    _this.props.handleNewLift(data);
			    return _this.setState(_this.state);
			  };
			})(this), 

			'JSON'
		);
	}


	handleValueChange(e){
		this.setState({
			[e.target.name]: e.target.value
		});
	}


	toggleUnit(e){
		e.preventDefault();
		this.setState({ ismetric: !this.state.ismetric });
	}


	valid(){
		return(
			this.state.date &&
			this.state.liftname &&
			this.state.weightlifted &&
			this.state.repsperformed &&
			this.state.onerm
		);
	}


	calculateOneRm(){
		this.state.onerm = this.state.weightlifted && this.state.repsperformed ?
			this.state.weightlifted / coefficients[this.state.repsperformed] :
			0;

		return this.state.onerm;
	}


	render () {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="row">
					
					<div className="col-sm-6 col-md-4">
						<input 
							type="date"
							className="form-control"
							placeholder="date"
							name="date"
							value={this.state.date}
							onChange={this.handleValueChange}
						/>
					</div>

					<div className="col-sm-6 col-md-4">
						<input 
							type="text"
							className="form-control"
							placeholder="liftname"
							name="liftname"
							value={this.state.liftname}
							onChange={this.handleValueChange}
						/>
					</div>

					<div className="col-sm-6 col-md-4">
						<button className="btn btn-primary" onClick={this.toggleUnit}>
							Metric = {this.state.ismetric.toString()}
						</button>
					</div>

					<div className="col-sm-6 col-md-4">
						<input 
							type="number"
							className="form-control"
							placeholder="weightlifted"
							name="weightlifted"
							value={this.state.weightlifted}
							onChange={this.handleValueChange}
						/>
					</div>

					<div className="col-sm-6 col-md-4">
						<input 
							type="number"
							min="1"
							max="10"
							className="form-control"
							placeholder="repsperformed"
							name="repsperformed"
							value={this.state.repsperformed}
							onChange={this.handleValueChange}
						/>
					</div>
				</div>

				<button type="submit" className="btn btn-primary" disabled={!this.valid()}>
					Create Lift
				</button>

				<OneRmBox
					onerm={this.calculateOneRm()}
				/>

			</form>
		);
	}
}


export default LiftForm