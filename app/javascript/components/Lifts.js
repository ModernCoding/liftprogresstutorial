import React from "react"
import PropTypes from "prop-types"
import Lift from "./Lift"
import LiftForm from "./LiftForm"

class Lifts extends React.Component {

	constructor(props){

		super(props);

		this.state = {
			lifts: this.props.data
		};

		this.myDefaultProps = {
			lifts: []
		};

		this.addLift = this.addLift.bind(this);
		this.updateLift = this.updateLift.bind(this);
		this.deleteLift = this.deleteLift.bind(this);
		
	}


	addLift(lift){
		let lifts = this.state.lifts.slice();
		lifts.push(lift);
		this.setState({ lifts: lifts });
	}


	updateLift(lift, data){
		let lifts = this.state.lifts.slice();
		let index = lifts.indexOf(lift);
		lifts[index] = data;
		this.setState({ lifts: lifts });
	}


	deleteLift(lift){
		let lifts = this.state.lifts.slice();
		let index = lifts.indexOf(lift);
		lifts.splice(index, 1);
		this.setState({ lifts: lifts });
	}


	render () {
		return(
			<div className="lifts">
				<h1 className="title">Lifts</h1>

				<LiftForm
					handleNewLift={this.addLift}
				/>
				
				<table className="table table-bordered">
					<thead>
						<tr>
							<th><i>Actions</i></th>
							<th>Date</th>
							<th>Lift name</th>
							<th>Weight lifted</th>
							<th>Reps performed</th>
							<th>1 RM</th>
							<th>Metric?</th>
						</tr>
					</thead>
					
					<tbody>
						{
							this.state.lifts.map(lift => {
								return(
									<Lift
										key={lift.id}
										lift={lift}
										handleEditLift={this.updateLift}
										handleDeleteLift={this.deleteLift}
									/>
								);
							})
						}
					</tbody>
				</table>
			</div>
		)
	}

}


export default Lifts