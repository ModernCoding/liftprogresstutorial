import React from "react"
import PropTypes from "prop-types"

class OneRmBox extends React.Component {
	render () {
		return (
			<div className="card">
				<div className="card-block">
					<h2 className="card-title text-center">
						1 RM Estimate
					</h2>
					
					<h3 className="card-text text-center">
						{this.props.onerm}
					</h3>
				</div>
			</div>
		);
	}
}

export default OneRmBox